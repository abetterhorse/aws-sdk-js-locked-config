var AWS = require("aws-sdk");

var nullCredentials = {};
Object.defineProperty( nullCredentials, 'awsAccessKeyId', {
  __proto__: null,
  value: null,
  writable: false,
  enumerable: true,
  configurable: false
});
Object.defineProperty( nullCredentials, 'awsSecretAccessKey', {
  __proto__: null,
  value: null,
  writable: false,
  enumerable: true,
  configurable: false
});
Object.defineProperty( nullCredentials, 'region', {
  __proto__: null,
  value: null,
  writable: false,
  enumerable: true,
  configurable: false
});
AWS.config = nullCredentials;

module.exports = AWS;
