var AWS = require( process.cwd() + "/index.js");
var assert = require("assert");

describe("AWS", awsDescription );

function awsDescription() {

  var keys = Object.keys( AWS.config );
  var property;
  for ( var index in keys ) {
    property = keys[ index ];

    assertDefaultIsNull( property );
    attemptAssignment( property );
  }
}

function assertDefaultIsNull( property ) {

  it("AWS.config." + property + " defaults to null", regionIsNull );

  function regionIsNull() {

    assert.ok(
      AWS.config[property] === null,
      "AWS.config." + property + " is not null"
    );
  }
}

function attemptAssignment( property ) {

  it("throws an error when AWS.config." + property + " is assigned a value", _attemptAssignment );
  function _attemptAssignment() {

    assert.throws(
      AWS.config[property] = "anything at all",
      "does not throw an error when AWS." + property + " is assigned a value"
    );
  }
}
